import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import JwtService from "@/core/services/jwt.service";

/**
 * Service to call HTTP request via Axios
 */
const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = "https://swapi.dev/api/";
  },

  /**
   * Set the default HTTP request headers
   */
  setHeader() {
    Vue.axios.defaults.headers.common[
      "Authorization"
    ] = `${JwtService.getToken()}`;
  },

  query(resource, params) {
    return Vue.axios.get(resource, params).catch(error => {
      // console.log(error);
      throw new Error(`[KT] ApiService ${error}`);
    });
  },

  /**
   * Send the GET HTTP request
   * @param resource
   * @param slug
   * @returns {*}
   */
  get(resource, params, contentType) {
    return Vue.axios.get(`${resource}`, params, contentType, {
      headers: {
        "Content-Type": contentType,
        Accept: "*/*",
        "Access-Control-Allow-Origin": "*"
      }
    });
  },

  /**
   * Set the POST HTTP request
   * @param resource
   * @param params
   * @returns {*}
   */
  post(resource, params, contentType) {
    return Vue.axios.post(`${resource}`, params, contentType, {
      headers: {
        "Content-Type": contentType,
        Accept: "*/*",
        "Access-Control-Allow-Origin": "*"
      }
    });
  },

  /**
   * Send the UPDATE HTTP request
   * @param resource
   * @param slug
   * @param params
   * @returns {IDBRequest<IDBValidKey> | Promise<void>}
   */
  update(resource, params, contentType) {
    return Vue.axios.put(`${resource}`, params, contentType, {
      headers: {
        "Content-Type": contentType,
        Accept: "*/*",
        "Access-Control-Allow-Origin": "*"
      }
    });
  },

  /**
   * Send the PUT HTTP request
   * @param resource
   * @param params
   * @returns {IDBRequest<IDBValidKey> | Promise<void>}
   */
  put(resource, contentType) {
    return Vue.axios.put(`${resource}`, contentType, {
      headers: {
        "Content-Type": contentType,
        Accept: "*/*",
        "Access-Control-Allow-Origin": "*"
      }
    });
  },

  /**
   * Send the DELETE HTTP request
   * @param resource
   * @returns {*}
   */
  delete(resource, contentType) {
    return Vue.axios.delete(`${resource}`, contentType, {
      headers: {
        "Content-Type": contentType,
        Accept: "*/*",
        "Access-Control-Allow-Origin": "*"
      }
    });
  }
};

export default ApiService;
